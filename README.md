# Write Backwards 1.2 #

A small app for Windows Phone. Transform text to reverse form and/or upside down. It supports multi-lined text. You can send sms and email right from the app. Text is also automatically copied for easy cut'n paste.
It was a personal project in 2012 after an introduction course to WP7 at the university. The most interesting part was the Windows Phone Store process.

I have no plans on further work on this. People keep using it, though, which is nice :)

## Windows Store ##
It is published. See pictures, more info and comments over at the store:
http://www.windowsphone.com/en-US/apps/0243d116-f48d-4d5a-968c-00be5eef7153

Reviews can be found here:
http://wp7reviews.tomverhoeff.com/AppReviews.aspx?id=0243d116-f48d-4d5a-968c-00be5eef7153


**Over 2250 downloaded (07.01.14) since 10.05.12.** 33 downloads in September. Not bad! 

![05.10.14.jpg](https://bitbucket.org/repo/xrdAjy/images/1419479653-05.10.14.jpg)