﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace WriteBackwards
{
    public partial class Page1 : PhoneApplicationPage
    {
        public Page1()
        {
            InitializeComponent();
        }

       /* private void textBlock8_Tap(object sender, GestureEventArgs e)
        {
            WebBrowserTask webbrowser = new WebBrowserTask();
            webbrowser.Uri("http://ekelundmobiles.im-name.net");
            webbrowser.Show();
        }*/

        private void textBlock7_Tap(object sender, GestureEventArgs e)
        {
            try
            {
                EmailComposeTask emailComposeTask = new EmailComposeTask();
                emailComposeTask.To = "ekelund@im-name.net";
                emailComposeTask.Subject = "Regarding WriteBackwards";
                emailComposeTask.Body = "";
                emailComposeTask.Show();
            }
            catch (Exception ex)
            {
                BugSense.BugSenseHandler.Instance.LogError(ex);
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();

        }
    }
}