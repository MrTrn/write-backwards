﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;

namespace WriteBackwards
{
    public static class StringExtention
    {
        public static string cFlip(this string s)
        {
            StringBuilder sb = new StringBuilder();
            String tmp;
            s = s.ToLowerInvariant(); // make sure it's lowercase before we start
            char[] charArray = s.ToCharArray();
            foreach (char theChar in charArray)
            {
                tmp = theChar.ToString();
                switch (tmp)
                {
                        // welcome to the switch case from hell..
                    case "\u0250":
                        {
                            sb.Append("a");
                            break;
                        }
                    case "\u0254":
                        {
                            sb.Append("c");
                            break;
                        }
                    case "\u01DD":
                        {
                            sb.Append("e");
                            break;
                        }
                    case "\u025F":
                        {
                            sb.Append("f");
                            break;
                        }
                    case "\u0183":
                        {
                            sb.Append("g");
                            break;
                        }
                    case "\u0265":
                        {
                            sb.Append("h");
                            break;
                        }
                    case "\u0131":
                        {
                            sb.Append("i");
                            break;
                        }
                    case "\u027E":
                        {
                            sb.Append("j");
                            break;
                        }
                    case "\u029E":
                        {
                            sb.Append("k");
                            break;
                        }
                    case "\u05DF":
                        {
                            sb.Append("l");
                            break;
                        }
                    case "\u026F":
                        {
                            sb.Append("m");
                            break;
                        }
                    case "d":
                        {
                            sb.Append("p");
                            break;
                        }
                    case "p":
                        {
                            sb.Append("d");
                            break;
                        }
                    case "b":
                        {
                            sb.Append("q");
                            break;
                        }
                    case "q":
                        {
                            sb.Append("b");
                            break;
                        }
                    case "\u0279":
                        {
                            sb.Append("r");
                            break;
                        }
                    case "\u0287":
                        {
                            sb.Append("t");
                            break;
                        }
                    case "n":
                        {
                            sb.Append("u");
                            break;
                        }
                    case "\u028C":
                        {
                            sb.Append("v");
                            break;
                        }
                    case "\u028D":
                        {
                            sb.Append("w");
                            break;
                        }
                    case "\u028E":
                        {
                            sb.Append("y");
                            break;
                        }
                    case "\u02D9":
                        {
                            sb.Append(".");
                            break;
                        }
                    case "\u00BF":
                        {
                            sb.Append("?");
                            break;
                        }
                    case "\u00A1":
                        {
                            sb.Append("!");
                            break;
                        }
                    case "[":
                        {
                            sb.Append("]");
                            break;
                        }
                    case "]":
                        {
                            sb.Append("[");
                            break;
                        }
                    case "(":
                        {
                            sb.Append(")");
                            break;
                        }
                    case ")":
                        {
                            sb.Append("(");
                            break;
                        }
                    case "{":
                        {
                            sb.Append("}");
                            break;
                        }
                    case "}":
                        {
                            sb.Append("{");
                            break;
                        }
                    case ",":
                        {
                            sb.Append("'");
                            break;
                        }
                    case ">":
                        {
                            sb.Append("<");
                            break;
                        }
                    case "\u203E":
                        {
                            sb.Append("_");
                            break;
                        }
                    case "\u201E":
                        {
                            sb.Append("\"");
                            break;
                        }
                    case "\u061B":
                        {
                            sb.Append(";");
                            break;
                        }
                    case "\u2040":
                        {
                            sb.Append("\u203F");
                            break;
                        }
                    case "\u2046":
                        {
                            sb.Append("\u2045");
                            break;
                        }
                    case "\u2235":
                        {
                            sb.Append("\u2234");
                            break;
                        }

                    // flip upside down
                    case "a":
                        {
                            sb.Append("\u0250");
                            break;
                        }
                    case "c":
                        {
                            sb.Append("\u0254");
                            break;
                        }
                    case "e":
                        {
                            sb.Append("\u01DD");
                            break;
                        }
                    case "f":
                        {
                            sb.Append("\u025F");
                            break;
                        }
                    case "g":
                        {
                            sb.Append("\u0183");
                            break;
                        }
                    case "h":
                        {
                            sb.Append("\u0265");
                            break;
                        }
                    case "i":
                        {
                            sb.Append("\u0131");
                            break;
                        }
                    case "j":
                        {
                            sb.Append("\u027E");
                            break;
                        }
                    case "k":
                        {
                            sb.Append("\u029E");
                            break;
                        }
                    case "l":
                        {
                            sb.Append("\u05DF");
                            break;
                        }
                    case "m":
                        {
                            sb.Append("\u026F");
                            break;
                        }
                    case "r":
                        {
                            sb.Append("\u0279");
                            break;
                        }
                    case "t":
                        {
                            sb.Append("\u0287");
                            break;
                        }
                    case "u":
                        {
                            sb.Append("n");
                            break;
                        }
                    case "v":
                        {
                            sb.Append("\u028C");
                            break;
                        }
                    case "w":
                        {
                            sb.Append("\u028D");
                            break;
                        }
                    case "y":
                        {
                            sb.Append("\u028E");
                            break;
                        }
                    case ".":
                        {
                            sb.Append("\u02D9");
                            break;
                        }
                    case "?":
                        {
                            sb.Append("\u00BF");
                            break;
                        }
                    case "!":
                        {
                            sb.Append("\u00A1");
                            break;
                        }
                    case "'":
                        {
                            sb.Append(",");
                            break;
                        }
                    case "<":
                        {
                            sb.Append(">");
                            break;
                        }
                    case "\\":
                        {
                            // \u0092
                            sb.Append("/");
                            break;
                        }
                    case "/":
                        {
                            sb.Append('\\');
                            break;
                        }
                    case "_":
                        {
                            sb.Append("\u203E");
                            break;
                        }
                    case "\"":
                        {
                            sb.Append("\u201E");
                            break;
                        }
                    case ";":
                        {
                            sb.Append("\u061B");
                            break;
                        }
                    case "\u203F":
                        {
                            sb.Append("\u2040");
                            break;
                        }
                    case "\u2045":
                        {
                            sb.Append("\u2046");
                            break;
                        }
                    case "\u2234":
                        sb.Append("\u2235");
                        break;
                    default:
                        {
                            // no matching char, just leave it as it is
                            sb.Append(tmp);
                            break;
                        }
                }
            }
            return sb.ToString();
        }
    }
}