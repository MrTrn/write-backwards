﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System.Diagnostics;

namespace WriteBackwards
{
    /* Write Backwards
     *  by Terje Rene Ekelund Nilsen - terje.nilsen@student.hive.no
     *  Coments in English - mostly for my own reference
     *  
     * 
     * */
    public partial class MainPage : PhoneApplicationPage
    {
        
// Constructor
        public MainPage()
        {  
            InitializeComponent();
            ;

            txtTheText.Text = "";
            txtTheText.Focus();
            /* // debug
             * 
             * adControl1.ApplicationId = "test_client";
             * adControl1.AdUnitId = "Image480_80"
             *  88139 29f22f30-a7f3-425f-9975-af5dcf9349c7
             * 
            adControl1.IsAutoRefreshEnabled = true;

            adControl1.IsAutoCollapseEnabled = true;*/
        }
        
        /* doMagic
         * execute the selected text manipulation
         * */
        private void doMagic_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTheText.Text != "")
                {
                    if (rbMakeBackwards.IsChecked == true)
                    {
                        txtTheText.Text = ReverseString(txtTheText.Text);
                    }
                    else 
                    {
                        txtTheText.Text = FlipText(ReverseString(txtTheText.Text));
                    }
                    Clipboard.SetText(txtTheText.Text);
                }
            }
            catch (Exception ex)
            {
                BugSense.BugSenseHandler.Instance.LogError(ex);
            }
        }

        /* ReverseString
         * Accepts: String
         * Returns: String
         * Reverses a given string.
         * Ex: ReverseString("123"); 
         * returns "321"
         * */
        public String ReverseString(String s)
        {
            try {
                char[] charArray = s.ToCharArray(); // add to array
                Array.Reverse(charArray); // reverse the array 
                return string.Join("", charArray); // ship it back
            }
            catch (Exception ex)
            {
                var overridenOptions = BugSense.BugSenseHandler.Instance.GetDefaultOptions();
                overridenOptions.Text = "Sorry!" + Environment.NewLine 
                    + "An error occoured." + Environment.NewLine 
                    + "Press OK to let us know, and we'll fix it :)";
                overridenOptions.Type = BugSense.enNotificationType.MessageBoxConfirm;

                BugSense.BugSenseHandler.Instance.LogError(ex,null,overridenOptions);
                return s;
            }
        }

        public String FlipText(String s)
        {
            try
            {
                return s.cFlip(); // flip letters upsidedown and back..
            }
            catch (Exception ex)
            {
                var overridenOptions = BugSense.BugSenseHandler.Instance.GetDefaultOptions();
                overridenOptions.Text = "Sorry!" + Environment.NewLine 
                    + "An error occoured." + Environment.NewLine 
                    + "Press OK to let us know, and we will fix it :)";
                overridenOptions.Type = BugSense.enNotificationType.MessageBoxConfirm;

                BugSense.BugSenseHandler.Instance.LogError(ex,null,overridenOptions);
                return s;
            }
        }

        private void showAbout_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));

        }

        private void sendSMS_Click(object sender, EventArgs e)
        {
            SmsComposeTask composeSMS = new SmsComposeTask();
            composeSMS.Body = txtTheText.Text;
            composeSMS.Show();
        }
        private void sendMAIL_Click(object sender, EventArgs e)
        {
            try
            {
                EmailComposeTask emailComposeTask = new EmailComposeTask();
                emailComposeTask.Body = txtTheText.Text;
                emailComposeTask.Show();
            }
            catch (Exception ex)
            {
                BugSense.BugSenseHandler.Instance.LogError(ex);
            }
        }
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            //
        }

        private void resetText_Click(object sender, EventArgs e)
        {
            txtTheText.Text = "";
        }

        private void rbFlipText_Checked(object sender, RoutedEventArgs e)
        {

        }

           
/*

        private void adControl1_BindingValidationError(object sender, ValidationErrorEventArgs e)
        {

            //
        }

        
        public void adControl1_AdRefreshed(object sender, EventArgs args)
        {

            AdControl ad = (AdControl)sender;



            Dispatcher.BeginInvoke(() =>
            {

                ad.Visibility = System.Windows.Visibility.Visible;

                Debug.WriteLine(

                  "ad control '" + ad.Name + "' got ad, visibility = " + ad.Visibility);


            });

        }



        private void adControl1_ErrorOccurred_1(object sender, Microsoft.Advertising.AdErrorEventArgs e)
        {

            try
            {

                AdControl ad = (AdControl)sender;



                Dispatcher.BeginInvoke(() =>
                {

                    ad.Visibility = System.Windows.Visibility.Collapsed;
                    String error = "error in ad control '" + ad.Name + "': " + e.Error.Message + "' visibility = " + ad.Visibility;

                    Debug.WriteLine(

                      "error in ad control '" + ad.Name + "': " + e.Error.Message);

                    Debug.WriteLine("ad control '" + ad.Name + "' visibility = " + ad.Visibility);
                    //BugSense.BugSenseHandler.Instance.LogError(e);
                });

            }

            catch (Exception ex)
            {

                Debug.WriteLine("oh no! " + ex.Message);
                BugSense.BugSenseHandler.Instance.LogError(ex);
            }

        }
        */

    }
}